import unittest

from app.gravity import update, will_collision
from app.vector import Vector


class MyTestCase(unittest.TestCase):
    def test_update(self):
        self.assertEqual(
            (Vector(-47, 24), Vector(1, 0)),
            update(Vector(-48, 24), Vector(0, 0)),
        )
        self.assertEqual(
            (Vector(33, -24), Vector(-5, 0)),
            update(Vector(38, -24), Vector(-4, 0)),
        )
        self.assertEqual(
            (Vector(70, -58), Vector(5, -4)),
            update(Vector(65, -54), Vector(5, -3) - Vector(-1, 1)),
        )

    def test_will_collision(self):
        self.assertEqual(
            4,
            will_collision(
                Vector(27, -24),
                Vector(-6, 0),
                400,
            ),
        )
        # 1ターンで死ぬ
        self.assertEqual(
            0,
            will_collision(
                Vector(1, 18),
                Vector(7, -3),
                400,
            ),
        )


if __name__ == '__main__':
    unittest.main()

import unittest

from app.vector import Vector
from inp.node import parse


class MyTestCase(unittest.TestCase):
    def test_eq(self):
        self.assertEqual(Vector(1, 2), Vector(1, 2))
        self.assertNotEqual(Vector(1, 2), Vector(1, 3))
        self.assertNotEqual(Vector(2, 1), Vector(1, 3))
        self.assertNotEqual(Vector(2, 1), Vector(3, 1))

    def test_add(self):
        self.assertEqual(Vector(1, 2) + Vector(3, 4), Vector(4, 6))
        self.assertEqual(Vector(1, 2) - Vector(3, 5), Vector(-2, -3))

    def test_mul(self):
        self.assertEqual(Vector(1, 2) * 3, Vector(3, 6))

    def test_as_node(self):
        self.assertEqual(str(Vector(2, 3).as_node()), '((cons, 2), 3)')

    def test_from_node(self):
        self.assertEqual(Vector.from_node(parse(iter('ap ap cons 2 3'.split()))), Vector(2, 3))

    def test_l1_norm(self):
        self.assertEqual(Vector(-3, 4).l1_norm(), 7)

    def test_linf_norm(self):
        self.assertEqual(Vector(-3, 4).linf_norm(), 4)
        self.assertEqual(Vector(-5, 4).linf_norm(), 5)


if __name__ == '__main__':
    unittest.main()

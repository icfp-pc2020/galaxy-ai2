import sys

import requests

from inp.node import modulate, demodulate, parse, as_list_cons, execute

__all__ = [
    'Client',
]


class Client:
    base_url = 'https://icfpc2020-api.testkontur.ru'
    api_key = '9b717496bbb64dd48c11c797be36155a'

    def post_alien(self, bits):
        return requests.post(self.base_url + '/aliens/send',
                             params={'apiKey': self.api_key},
                             data=bits)

    def send(self, data):
        data = execute({}, data)
        bits = ''.join(modulate(data))
        res = self.post_alien(bits)
        print(res)
        return as_list_cons(demodulate(res.text))


def main():
    token = sys.argv[1]
    bits = ''.join(modulate(parse(iter(token.split()))))
    res = Client().post_alien(bits)
    print(token)
    print(as_list_cons(demodulate(res.text)))

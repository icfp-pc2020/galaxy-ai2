from typing import List, Union, Dict

__all__ = [
    'Node',
    'Num',
    'Key',
    'Ap',
    'ListCons',
    'NodeType',
    'parse',
    'execute',
    'modulate',
    'demodulate',
    'as_list_cons',
]


class Node:
    def __init__(self, typ: str):
        self.typ = typ
        self.res = None

    def __str__(self):
        return self.typ

    __repr__ = __str__

    def __eq__(self, other):
        return self.typ == other.typ

    def copy(self):
        return Node(self.typ)


class Num(Node):
    def __init__(self, n: int):
        super(Num, self).__init__('num')
        self.n = n

    def __str__(self):
        return str(self.n)

    __repr__ = __str__

    def __eq__(self, other):
        return super(Num, self).__eq__(other) and self.n == other.n

    def copy(self):
        return Num(self.n)


class Key(Node):
    def __init__(self, key):
        super(Key, self).__init__('key')
        self.key = key

    def __str__(self):
        return f':{self.key}'

    __repr__ = __str__

    def __eq__(self, other):
        return super(Key, self).__eq__(other) and self.key == other.key

    def copy(self):
        return Key(self.key)


class Ap(Node):
    def __init__(self, left: Node, right: Node):
        super(Ap, self).__init__('ap')
        self.left = left
        self.right = right

    def __str__(self):
        return f'({self.left}, {self.right})'

    __repr__ = __str__

    def __eq__(self, other):
        return super(Ap, self).__eq__(other) and self.left == other.left and self.right == other.right

    def copy(self):
        return Ap(self.left.copy(), self.right.copy())


class ListCons(Node):
    def __init__(self, lis: List['NodeType']):
        super(ListCons, self).__init__('list_cons')
        self.lis = lis[::-1]

    def push_front(self, node) -> None:
        self.lis.append(node)

    def pop_front(self) -> 'NodeType':
        return self.lis.pop()

    def as_list(self) -> List['NodeType']:
        return self.lis[::-1]

    def __str__(self):
        return '[%s]' % ', '.join(map(str, self.lis[::-1]))

    __repr__ = __str__

    def __eq__(self, other):
        return super(ListCons, self).__eq__(other) and self.lis == other.lis

    def copy(self):
        node = ListCons([])
        node.lis = [n.copy() for n in self.lis]
        return node


NodeType = Union[
    Node,
    Num,
    Key,
    Ap,
    ListCons,
]


def parse(tokens) -> NodeType:
    t = next(tokens)
    if t == 'ap':
        return Ap(parse(tokens), parse(tokens))
    if t[0] == ':':
        return Key(t)
    try:
        return Num(int(t))
    except ValueError:
        return Node(t)


def execute(defs: Dict[str, NodeType], node: NodeType) -> NodeType:
    if node.res is not None:
        return node.res
    init = node
    while True:
        res = _execute(defs, node)
        if res == node:
            init.res = res
            return res
        node = res


def _execute(defs, node):
    if isinstance(node, Key):
        return defs[node.key]
    if isinstance(node, ListCons):
        # 展開しなくてもいい気もするが一応やる
        if node.lis:
            return Ap(Ap(Node('cons'), node.pop_front()), node)
        else:
            return Node('nil')
    if isinstance(node, Ap):
        func1 = execute(defs, node.left)
        x = node.right
        if isinstance(func1, Ap):
            func2 = execute(defs, func1.left)
            y = func1.right
            if isinstance(func2, Ap):
                func3 = execute(defs, func2.left)
                z = func2.right
                if func3.typ == 's':
                    return Ap(Ap(z, x), Ap(y, x))
                if func3.typ == 'c':
                    return Ap(Ap(z, x), y)
                if func3.typ == 'b':
                    return Ap(z, Ap(y, x))
                if func3.typ == 'if0':
                    if execute(defs, z).n == 0:
                        return y
                    else:
                        return x
                if func3.typ == 'cons':
                    return Ap(Ap(x, z), y)
            if func2.typ == 'add':
                left = execute(defs, y).n
                right = execute(defs, x).n
                return Num(left + right)
            if func2.typ == 'mul':
                left = execute(defs, y).n
                right = execute(defs, x).n
                return Num(left * right)
            if func2.typ == 'div':
                left = execute(defs, y).n
                right = execute(defs, x).n
                d = abs(left) // abs(right)
                if left * right >= 0:
                    return Num(d)
                else:
                    return Num(-d)
            if func2.typ == 'eq':
                left = execute(defs, y).n
                right = execute(defs, x).n
                if left == right:
                    return Node('t')
                else:
                    return Node('f')
            if func2.typ == 'lt':
                left = execute(defs, y).n
                right = execute(defs, x).n
                if left < right:
                    return Node('t')
                else:
                    return Node('f')
            if func2.typ == 't':
                return y
            if func2.typ == 'f':
                return x
            if func2.typ == 'cons':
                y = execute(defs, y)
                x = execute(defs, x)
                node = Ap(Ap(Node('cons'), y), x)
                node.res = node
                return node
        if func1.typ == 'inc':
            return Num(execute(defs, x).n + 1)
        if func1.typ == 'dec':
            return Num(execute(defs, x).n - 1)
        if func1.typ == 'neg':
            return Num(-execute(defs, x).n)
        if func1.typ == 'pwr2':
            return Num(2 ** execute(defs, x).n)
        if func1.typ == 'i':
            return x
        if func1.typ == 'isnil':
            return Ap(x, Ap(Node('t'), Ap(Node('t'), Node('f'))))
        if func1.typ == 'car':
            return Ap(x, Node('t'))
        if func1.typ == 'cdr':
            return Ap(x, Node('f'))
        if func1.typ == 'nil':
            return Node('t')
    return node


def modulate(node: Node) -> List[str]:
    if node.typ == 'nil':
        return ['00']
    if node.typ == 'cons':
        return ['11']
    if isinstance(node, Ap):
        lis = modulate(node.left)
        lis.extend(modulate(node.right))
        return lis
    assert isinstance(node, Num), node
    if node.n > 0:
        s = '01'
        n = node.n
    elif node.n < 0:
        s = '10'
        n = -node.n
    else:
        return ['010']
    bit = bin(n)[2:]
    bit = '0' * (4 - len(bit) % 4) + bit
    bits = 0
    while bits * 4 < len(bit):
        bits += 1
    return [s + '1' * bits + '0' + bit]


def demodulate(s) -> Node:
    if isinstance(s, str):
        s = iter(s)
    a = next(s)
    b = next(s)
    if (a, b) == ('0', '0'):
        return Node('nil')
    if (a, b) == ('1', '1'):
        return Ap(Ap(Node('cons'), demodulate(s)), demodulate(s))
    bits = 0
    while next(s) == '1':
        bits += 1
    if bits == 0:
        n = 0
    else:
        n = int(''.join(next(s) for _ in range(bits * 4)), 2)
    if a == '0':
        return Num(n)
    else:
        return Num(-n)


def as_list_cons(node: NodeType) -> NodeType:
    node = node.copy()
    if isinstance(node, Ap):
        if isinstance(node.left, Ap):
            if node.left.left.typ == 'cons':
                right = as_list_cons(node.right)
                if isinstance(right, ListCons):
                    right.push_front(as_list_cons(node.left.right))
                    return right
                if right.typ == 'nil':
                    return ListCons([as_list_cons(node.left.right)])
        node.left = as_list_cons(node.left)
        node.right = as_list_cons(node.right)
    return node

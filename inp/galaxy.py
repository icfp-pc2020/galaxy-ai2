import sys
from pathlib import Path
from typing import Dict, Tuple

from inp.alien import Client
from inp.node import *

if sys.getrecursionlimit() < 10 ** 4:
    sys.setrecursionlimit(10 ** 4)

__all__ = [
    'parse_galaxy',
    'GALAXY',
    'as_click',
    'interact',
]

statelessdraw = 'ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b ' \
                'cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil'
statefuldraw = 'ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b ' \
               'cons ap ap c cons nil ap ap c cons nil ap c cons'


def parse_galaxy() -> Dict[str, NodeType]:
    defs = {}
    for line in (Path(__file__).parent / 'galaxy.txt').open():
        name, _, *tokens = line.split()
        defs[name] = parse(iter(tokens))
    defs['statelessdraw'] = parse(iter(statelessdraw.split()))
    defs['statefuldraw'] = parse(iter(statefuldraw.split()))
    return defs


GALAXY = parse_galaxy()


def as_click(vec: Tuple[int, int]) -> NodeType:
    return Ap(Ap(Node('cons'), Num(vec[0])), Num(vec[1]))


def interact(protocol: str, state: NodeType, event: NodeType) -> Tuple[NodeType, NodeType]:
    while True:
        # これをすると何故か動く
        state = execute({}, state)
        node = execute(GALAXY, Ap(Ap(Key(protocol), state), event))
        flag, new_state, data = as_list_cons(node).as_list()
        if flag == Num(0):
            return new_state, data
        print('flag:', flag)
        state = new_state
        event = send_to_alien_proxy(data)


def send_to_alien_proxy(data: NodeType) -> NodeType:
    return Client().send(data)

from typing import Tuple

from inp.node import *

__all__ = [
    'as_vec',
    'draw',
]


def as_vec(node: NodeType) -> Tuple[int, int]:
    assert isinstance(node, Ap), node
    assert isinstance(node.left, Ap), node.left
    assert isinstance(node.left.right, Num), node.left.right
    assert isinstance(node.right, Num), node.right
    return node.left.right.n, node.right.n


def draw(lis: ListCons):
    return [as_vec(v) for v in lis.as_list()]

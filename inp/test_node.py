import unittest

from inp.node import *


def exe(s: str, defs=None) -> Node:
    tokens = iter(s.split() + ['!!!!'])
    node = parse(tokens)
    assert next(tokens) == '!!!!'
    return execute(defs, node)


class NumberTest(unittest.TestCase):
    def test_neg(self):
        self.assertEqual(exe('ap neg 0'), Num(0))
        self.assertEqual(exe('ap neg 1'), Num(-1))
        self.assertEqual(exe('ap neg 2'), Num(-2))

    def test_inc_dec(self):
        self.assertEqual(exe('ap inc 0'), Num(1))
        self.assertEqual(exe('ap inc 1'), Num(2))
        self.assertEqual(exe('ap inc -2'), Num(-1))
        self.assertEqual(exe('ap dec 0'), Num(-1))
        self.assertEqual(exe('ap dec 1'), Num(0))
        self.assertEqual(exe('ap dec -2'), Num(-3))

    def test_sum(self):
        self.assertEqual(exe('ap ap add 1 2'), Num(3))
        self.assertEqual(exe('ap ap add 2 1'), Num(3))
        self.assertEqual(exe('ap ap add 0 1'), Num(1))
        self.assertEqual(exe('ap ap add 2 3'), Num(5))

    def test_mul(self):
        self.assertEqual(exe('ap ap mul 4 2'), Num(8))
        self.assertEqual(exe('ap ap mul 3 -2'), Num(-6))

    def test_div(self):
        self.assertEqual(exe('ap ap div 4 2'), Num(2))
        self.assertEqual(exe('ap ap div 4 3'), Num(1))
        self.assertEqual(exe('ap ap div 4 4'), Num(1))
        self.assertEqual(exe('ap ap div 4 5'), Num(0))
        self.assertEqual(exe('ap ap div 5 2'), Num(2))
        self.assertEqual(exe('ap ap div 6 -2'), Num(-3))
        self.assertEqual(exe('ap ap div 5 -3'), Num(-1))
        self.assertEqual(exe('ap ap div -5 3'), Num(-1))
        self.assertEqual(exe('ap ap div -5 -3'), Num(1))

    def test_eq(self):
        self.assertEqual(exe('ap ap eq 0 -2'), Node('f'))
        self.assertEqual(exe('ap ap eq 0 -1'), Node('f'))
        self.assertEqual(exe('ap ap eq 0 0'), Node('t'))
        self.assertEqual(exe('ap ap eq 0 1'), Node('f'))
        self.assertEqual(exe('ap ap eq 0 2'), Node('f'))
        self.assertEqual(exe('ap ap eq 1 -1'), Node('f'))
        self.assertEqual(exe('ap ap eq 1 0'), Node('f'))
        self.assertEqual(exe('ap ap eq 1 1'), Node('t'))
        self.assertEqual(exe('ap ap eq 1 2'), Node('f'))
        self.assertEqual(exe('ap ap eq 1 3'), Node('f'))
        self.assertEqual(exe('ap ap eq 2 0'), Node('f'))
        self.assertEqual(exe('ap ap eq 2 1'), Node('f'))
        self.assertEqual(exe('ap ap eq 2 2'), Node('t'))
        self.assertEqual(exe('ap ap eq 2 3'), Node('f'))
        self.assertEqual(exe('ap ap eq 2 4'), Node('f'))
        self.assertEqual(exe('ap ap eq 19 20'), Node('f'))
        self.assertEqual(exe('ap ap eq 20 20'), Node('t'))
        self.assertEqual(exe('ap ap eq 21 20'), Node('f'))
        self.assertEqual(exe('ap ap eq -19 -20'), Node('f'))
        self.assertEqual(exe('ap ap eq -20 -20'), Node('t'))
        self.assertEqual(exe('ap ap eq -21 -20'), Node('f'))

    def test_lt(self):
        self.assertEqual(exe('ap ap lt 0 -1'), Node('f'))
        self.assertEqual(exe('ap ap lt 0 0'), Node('f'))
        self.assertEqual(exe('ap ap lt 0 1'), Node('t'))
        self.assertEqual(exe('ap ap lt 0 2'), Node('t'))
        self.assertEqual(exe('ap ap lt 1 0'), Node('f'))
        self.assertEqual(exe('ap ap lt 1 1'), Node('f'))
        self.assertEqual(exe('ap ap lt 1 2'), Node('t'))
        self.assertEqual(exe('ap ap lt 1 3'), Node('t'))
        self.assertEqual(exe('ap ap lt 2 1'), Node('f'))
        self.assertEqual(exe('ap ap lt 2 2'), Node('f'))
        self.assertEqual(exe('ap ap lt 2 3'), Node('t'))
        self.assertEqual(exe('ap ap lt 2 4'), Node('t'))
        self.assertEqual(exe('ap ap lt 19 20'), Node('t'))
        self.assertEqual(exe('ap ap lt 20 20'), Node('f'))
        self.assertEqual(exe('ap ap lt 21 20'), Node('f'))
        self.assertEqual(exe('ap ap lt -19 -20'), Node('f'))
        self.assertEqual(exe('ap ap lt -20 -20'), Node('f'))
        self.assertEqual(exe('ap ap lt -21 -20'), Node('t'))

    def test_pow2(self):
        self.assertEqual(exe('ap pwr2 0'), Num(1))
        self.assertEqual(exe('ap pwr2 1'), Num(2))
        self.assertEqual(exe('ap pwr2 3'), Num(8))


class CombinatorTest(unittest.TestCase):
    def test_s(self):
        self.assertEqual(exe('ap ap ap s x0 x1 x2'), parse(iter('ap ap x0 x2 ap x1 x2'.split())))
        self.assertEqual(exe('ap ap ap s add inc 1'), Num(3))
        self.assertEqual(exe('ap ap ap s mul ap add 1 6'), Num(42))

    def test_c(self):
        self.assertEqual(exe('ap ap ap c x0 x1 x2'), parse(iter('ap ap x0 x2 x1'.split())))
        self.assertEqual(exe('ap ap ap c add 1 2'), Num(3))

    def test_b(self):
        self.assertEqual(exe('ap ap ap b x0 x1 x2'), parse(iter('ap x0 ap x1 x2'.split())))
        for i in range(-10, 10):
            self.assertEqual(exe(f'ap ap ap b inc dec {i}'), Num(i))

    def test_true(self):
        self.assertEqual(exe('ap ap t x0 x1'), Node('x0'))
        self.assertEqual(exe('ap ap t 1 5'), Num(1))
        self.assertEqual(exe('ap ap t t i'), Node('t'))
        self.assertEqual(exe('ap ap t t ap inc 5'), Node('t'))
        self.assertEqual(exe('ap ap t ap inc 5 t'), Num(6))

    def test_false(self):
        self.assertEqual(exe('ap ap f x0 x1'), Node('x1'))

    def test_i(self):
        self.assertEqual(exe('ap i x0'), Node('x0'))
        self.assertEqual(exe('ap i 1'), Num(1))
        self.assertEqual(exe('ap i i'), Node('i'))
        self.assertEqual(exe('ap i add'), Node('add'))
        self.assertEqual(exe('ap i ap add 1'), parse(iter('ap add 1'.split())))


class LispTest(unittest.TestCase):

    def test_con(self):
        self.assertEqual(exe('ap ap ap cons x0 x1 x2'), parse(iter('ap ap x2 x0 x1'.split())))

    def test_car(self):
        self.assertEqual(exe('ap car ap ap cons x0 x1'), Node('x0'))
        self.assertEqual(exe('ap car x2'), parse(iter('ap x2 t'.split())))

    def test_cdr(self):
        self.assertEqual(exe('ap cdr ap ap cons x0 x1'), parse(iter('x1'.split())))
        self.assertEqual(exe('ap cdr x2'), parse(iter('ap x2 f'.split())))

    def test_nil(self):
        self.assertEqual(exe('ap nil x0'), Node('t'))

    def test_is_nil(self):
        self.assertEqual(exe('ap isnil nil'), Node('t'))
        self.assertEqual(exe('ap isnil ap ap cons x0 x1'), Node('f'))


class ControlText(unittest.TestCase):

    def test_is_zero(self):
        self.assertEqual(exe('ap ap ap if0 0 x0 x1'), Node('x0'))
        self.assertEqual(exe('ap ap ap if0 1 x0 x1'), Node('x1'))


# class InteractTest(unittest.TestCase):
#
#     def setUp(self) -> None:
#         statelessdraw = 'ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ' \
#                         'ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil'
#         self.defs = {
#             1: parse(iter(statelessdraw.split())),
#         }
#
#     def test_statelessdraw_def(self):
#         s = 'ap ap cons 0 ap ap cons nil ap ap cons ap ap cons x1 nil nil'
#         self.assertEqual(exe('ap ap :1 x0 x1', self.defs), parse(iter(s.split())))


class ModDemTest(unittest.TestCase):

    def test_mod_num(self):
        self.assertEqual(modulate(Num(0)), ['010'])
        self.assertEqual(modulate(Num(1)), ['01100001'])
        self.assertEqual(modulate(Num(-1)), ['10100001'])
        self.assertEqual(modulate(Num(2)), ['01100010'])
        self.assertEqual(modulate(Num(16)), ['0111000010000'])
        self.assertEqual(modulate(Num(18)), ['0111000010010'])

    def test_dem_num(self):
        self.assertEqual(demodulate('010'), Num(0))
        self.assertEqual(demodulate('01100001'), Num(1))
        self.assertEqual(demodulate('10100001'), Num(-1))
        self.assertEqual(demodulate('01100010'), Num(2))
        self.assertEqual(demodulate('0111000010000'), Num(16))
        self.assertEqual(demodulate('0111000010010'), Num(18))

    def test_mod_list(self):
        self.assertEqual(modulate(parse(iter('nil'.split()))), ['00'])
        self.assertEqual(modulate(parse(iter('ap ap cons nil nil'.split()))), ['11', '00', '00'])
        self.assertEqual(modulate(parse(iter('ap ap cons 1 nil'.split()))), ['11', '01100001', '00'])
        self.assertEqual(modulate(parse(iter('ap ap cons 1 2'.split()))), ['11', '01100001', '01100010'])
        self.assertEqual(modulate(parse(iter('ap ap cons 1 ap ap cons 2 nil'.split()))),
                         ['11', '01100001', '11', '01100010', '00'])

    def test_dem_list(self):
        self.assertEqual(demodulate('00'), Node('nil'))
        self.assertEqual(demodulate('110000'), parse(iter('ap ap cons nil nil'.split())))
        self.assertEqual(demodulate('110110000100'), parse(iter('ap ap cons 1 nil'.split())))
        self.assertEqual(demodulate('110110000101100010'), parse(iter('ap ap cons 1 2'.split())))
        self.assertEqual(demodulate('1101100001110110001000'), parse(iter('ap ap cons 1 ap ap cons 2 nil'.split())))


class ListConsTest(unittest.TestCase):

    def test_node(self):
        lis = ListCons([Num(2)])
        lis.push_front(Num(1))
        self.assertEqual(lis, ListCons([Num(1), Num(2)]))
        self.assertEqual(lis.as_list(), [Num(1), Num(2)])

    def test_simple(self):
        self.assertEqual(as_list_cons(parse(iter('ap ap cons 1 nil'.split()))), ListCons([Num(1)]))
        self.assertEqual(as_list_cons(parse(iter('ap ap cons 1 ap ap cons 2 nil'.split()))),
                         ListCons([Num(1), Num(2)]))
        self.assertEqual(as_list_cons(parse(iter('ap ap cons 1 ap ap cons 2 3'.split()))),
                         parse(iter('ap ap cons 1 ap ap cons 2 3'.split())))
        self.assertEqual(as_list_cons(parse(iter('ap ap cons 1 ap ap cons 2 3'.split()))),
                         parse(iter('ap ap cons 1 ap ap cons 2 3'.split())))

    def test_nest(self):
        self.assertEqual(as_list_cons(parse(iter('ap ap cons ap ap cons 1 nil nil'.split()))),
                         ListCons([ListCons([Num(1)])]))
        self.assertEqual(as_list_cons(parse(iter('ap ap cons ap ap cons 1 nil nil'.split()))),
                         ListCons([ListCons([Num(1)])]))


if __name__ == '__main__':
    unittest.main()

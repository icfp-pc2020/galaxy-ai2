from inp.node import Ap, Node, Num
from inp.utils import as_vec

__all__ = [
    'Vector',
    'sign',
]


def sign(x: int) -> int:
    if x > 0:
        return 1
    elif x < 0:
        return -1
    return 0


class Vector:
    __slots__ = ('x', 'y')

    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __add__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, other: int) -> 'Vector':
        return Vector(self.x * other, self.y * other)

    def __neg__(self) -> 'Vector':
        return Vector(-self.x, -self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __repr__(self):
        return f'{self.__class__.__name__}({self.x}, {self.y})'

    def __str__(self):
        return f'({self.x}, {self.y})'

    def as_node(self) -> Ap:
        return Ap(Ap(Node('cons'), Num(self.x)), Num(self.y))

    @classmethod
    def from_node(cls, node):
        x, y = as_vec(node)
        return cls(x, y)

    def l1_norm(self) -> int:
        return abs(self.x) + abs(self.y)

    def linf_norm(self) -> int:
        return max(abs(self.x), abs(self.y))

    def sign(self) -> 'Vector':
        return Vector(sign(self.x), sign(self.y))

import sys
from typing import Tuple

import requests

from app.vector import Vector
from inp.node import *

server_url = ''
player_key = ''


def as_bit(node):
    print(node)
    node = execute({}, node)
    # print(node)
    return ''.join(modulate(node))


def req(node):
    print('try request:')
    bits = as_bit(node)
    print(bits)
    res = requests.post(server_url, data=bits)
    if res.status_code != 200:
        print('Unexpected server response')
        print('HTTP code:', res.status_code)
        print('Response body:', res.text)
        exit(2)
    print('suc')
    return res


class StageInfo:
    def __init__(self, info: Num):
        if info.n == 0:
            self.stage = 'yet'
        elif info.n == 1:
            self.stage = 'started'
        elif info.n == 2:
            self.stage = 'finished'
        else:
            self.stage = f'unknown:{info.n}'


class GameInfo:
    def __init__(self, info: ListCons):
        """
        Def: staticGameInfo = (x0, role, x2, x3, x4)
        例: [256, 0, [512, 1, 64], [16, 128], [1, 2, 3, 4]]
        """
        x0, role, x2, x3, x4 = info.as_list()
        self.x0 = x0.n
        self.role = role.n
        self.x2 = x2
        self.x3 = x3
        self.x4 = x4

    def is_attacker(self) -> bool:
        return self.role == 0

    def __str__(self):
        return ' '.join([
            f'x0={self.x0}',
            f'role={self.role}',
            f'x2={self.x2}',
            f'x3={self.x3}',
            f'x4={self.x4}',
        ])


class GameState:
    def __init__(self, info: ListCons):
        _game_tick, _x1, _ship_commands = info.as_list()
        self.game_tick = _game_tick.n
        self.x1 = _x1
        if _ship_commands == Node('nil'):
            _ship_commands = []
        else:
            _ship_commands = as_list_cons(_ship_commands).as_list()
        self.ships = []
        self.applied_commands = []
        for sc in _ship_commands:
            ship, cmd = sc.as_list()
            self.ships.append(Ship(ship))
            # TODO impl Applied Command
            self.applied_commands.append(cmd)


class Ship:
    def __init__(self, info: ListCons):
        _role, _ship_id, _position, _velocity, _x4, _x5, _x6, _x7 = info.as_list()
        self.role = _role.n
        self.ship_id = _ship_id.n
        self.position = Vector.from_node(_position)
        self.velocity = Vector.from_node(_velocity)
        self.parameter = ShipParameter.from_node(_x4)
        self.tired = _x5.n
        self.max_capacity = _x6.n
        self.x7 = _x7.n


class ShipParameter:
    def __init__(self, fuel: int, power: int, regeneration: int, p4=1):
        self.fuel = fuel
        self.power = power
        self.regeneration = regeneration
        self.p4 = p4

    @classmethod
    def from_node(cls, x4: ListCons) -> 'ShipParameter':
        p1, p2, p3, p4 = x4.as_list()
        return cls(p1.n, p2.n, p3.n, p4.n)

    def as_node(self) -> ListCons:
        return ListCons([
            Num(self.fuel),
            Num(self.power),
            Num(self.regeneration),
            Num(self.p4),
        ])

    def __str__(self):
        return ' '.join([
            f'fuel={self.fuel}',
            f'power={self.power}',
            f'reg={self.regeneration}',
            f'p4={self.p4}',
        ])


def parse_game_res(node) -> Tuple[StageInfo, GameInfo, GameState]:
    lis = as_list_cons(node).as_list()
    if len(lis) == 1:
        print('invalid:', lis)
        exit(2)
    _, stage, info, state = lis
    return StageInfo(stage), GameInfo(info), GameState(state)


def run(ai_cls):
    global server_url, player_key
    server_url = sys.argv[1] + '/aliens/send'
    player_key = int(sys.argv[2])
    print(f'server_url={server_url}')
    print(f'player_key={player_key}')

    # JOIN
    res = req(ListCons([
        Num(2),
        Num(player_key),
        Node('nil'),
    ]))
    print('JOIN response:', res.text)
    node = demodulate(res.text)
    print('JOIN demodulate:', node)
    _, stage, info, _ = as_list_cons(node).as_list()
    stage = StageInfo(as_list_cons(stage))
    # submit 時のテスト
    if stage.stage == 'finished':
        return
    info = GameInfo(as_list_cons(info))

    # START
    res = req(ListCons([
        Num(3),
        Num(player_key),
        ai_cls.init_ship_parameter(info.role).as_node(),
    ]))
    print('START response:', res.text)
    node = demodulate(res.text)
    print('START demodulate:', node)
    stage, info, state = parse_game_res(node)

    ai = ai_cls(stage, info, state)

    # COMMANDS
    while True:
        commands = ai.think()
        res = req(ListCons([
            Num(4),
            Num(player_key),
            ListCons([cmd.as_node() for cmd in commands]),
        ]))
        # print('COMMANDS response:', res.text)
        node = demodulate(res.text)
        # print('COMMANDS demodulate:', node)
        stage, info, state = parse_game_res(node)
        ai.update(state)

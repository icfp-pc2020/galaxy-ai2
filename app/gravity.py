from typing import Tuple

from app.vector import Vector, sign

__all__ = [
    'update',
    'will_collision',
]


def update(position: Vector, velocity: Vector) -> Tuple[Vector, Vector]:
    x = abs(position.x)
    y = abs(position.y)
    m = max(x, y)
    ax = ay = 0
    if x == m:
        ax = -sign(position.x)
    if y == m:
        ay = -sign(position.y)
    v = velocity + Vector(ax, ay)
    p = position + v
    return p, v


def will_collision(
        position: Vector,
        velocity: Vector,
        tick: int,
        *,
        radius=16,
        max_radius=128,
) -> int:
    for i in range(tick):
        position, velocity = update(position, velocity)
        n = position.linf_norm()
        if n <= radius or max_radius <= n:
            return i
    return tick

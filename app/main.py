from random import shuffle
from typing import List

from app.game import (
    run,
    StageInfo,
    GameInfo,
    GameState,
    Ship,
    ShipParameter,
)
from app.gravity import will_collision, update
from app.vector import Vector
from inp.node import ListCons, Num, NodeType


def sign(x: int) -> int:
    if x >= 0:
        return 1
    return -1


class CommandBase:

    def __init__(self, ship_id: int):
        self.ship_id = ship_id

    def as_node(self) -> ListCons:
        raise NotImplementedError


class Accelerate(CommandBase):

    def __init__(self, ship_id: int, vector: Vector):
        super().__init__(ship_id)
        self.vector = vector

    def as_node(self) -> ListCons:
        return ListCons([
            Num(0),
            Num(self.ship_id),
            self.vector.as_node(),
        ])


class Detonate(CommandBase):

    def as_node(self) -> ListCons:
        return ListCons([
            Num(1),
            Num(self.ship_id),
        ])


class Shoot(CommandBase):

    def __init__(self, ship_id: int, target: Vector, x3: NodeType):
        super().__init__(ship_id)
        self.target = target
        self.x3 = x3

    def as_node(self) -> ListCons:
        return ListCons([
            Num(2),
            Num(self.ship_id),
            self.target.as_node(),
            self.x3,
        ])


class Separate(CommandBase):

    def __init__(self, ship_id: int, params: ShipParameter):
        super().__init__(ship_id)
        self.params = params

    def as_node(self) -> ListCons:
        return ListCons([
            Num(3),
            Num(self.ship_id),
            self.params.as_node()
        ])


class AiBase:

    def __init__(self, stage: StageInfo, info: GameInfo, state: GameState) -> None:
        self.stage_info = stage
        self.game_info = info
        self.state = state

    def think(self) -> List[CommandBase]:
        raise NotImplementedError

    def update(self, state: GameState) -> None:
        self.state = state

    @classmethod
    def init_ship_parameter(cls, role: int) -> ShipParameter:
        if role == 0:
            # 攻撃側
            return ShipParameter(
                fuel=256,
                power=10,
                regeneration=10,
            )
        else:
            # 防御側
            return ShipParameter(
                fuel=256,
                power=10,
                regeneration=10,
            )


class KeepStaticMixin:

    @staticmethod
    def think_ship(ship: Ship):
        def f48(x: int) -> int:
            if x == 48:
                return -1
            if x == -48:
                return 1
            return 0

        d = Vector(f48(ship.position.x), f48(ship.position.y))
        return Accelerate(ship.ship_id, d)


class SpaceMineAi(AiBase, KeepStaticMixin):

    def think(self) -> List[CommandBase]:
        commands = []
        other_pos_list = []
        for ship in self.state.ships:
            if self.game_info.role == ship.role:
                commands.append(self.think_ship(ship))
            else:
                other_pos_list.append(ship.position + ship.velocity)
        for ship in self.state.ships:
            if self.game_info.role == ship.role:
                if self.is_detonate(ship.position, other_pos_list):
                    commands.append(Detonate(ship.ship_id))
        return commands

    @staticmethod
    def is_detonate(self_pos, other_pos_list) -> bool:
        return all((self_pos - p).linf_norm() <= 10 for p in other_pos_list)


class NoLookAnotherAiBase(AiBase):

    def think(self) -> List[CommandBase]:
        commands = []
        for ship in self.state.ships:
            if self.game_info.role == ship.role:
                cmd = self.think_ship(ship)
                if cmd is not None:
                    commands.append(cmd)
        return commands

    def think_ship(self, ship: Ship):
        raise NotImplementedError


class OutGoingAi(NoLookAnotherAiBase):

    def think_ship(self, ship: Ship):
        return Accelerate(ship.ship_id, ship.position.sign())


class KeepStaticAi(NoLookAnotherAiBase, KeepStaticMixin):
    pass


class InoueKuruKuruAi(AiBase):

    def __init__(self, stage: StageInfo, info: GameInfo, state: GameState) -> None:
        super().__init__(stage, info, state)
        self.kurukuru_state = 0

    def think(self) -> List[CommandBase]:
        if self.kurukuru_state >= 12:
            return []

        commands = []
        for ship in self.state.ships:
            if self.game_info.role == ship.role:
                if self.kurukuru_state < 7:
                    commands.append(self.kasoku(ship))
                else:
                    commands.append(self.flying_kasoku(ship))
        return commands

    def flying_kasoku(self, ship: Ship):
        # 正方形から飛び出したあとも勢いづけを行う
        self.kurukuru_state += 1
        if ship.velocity.x == 0:
            d = Vector(sign(ship.position.x), -sign(ship.velocity.y))
        elif ship.velocity.y == 0:
            d = Vector(-sign(ship.velocity.x), sign(ship.position.y))
        else:
            d = Vector(-sign(ship.velocity.x), -sign(ship.velocity.y))
        return Accelerate(ship.ship_id, d)

    def kasoku(self, ship: Ship):
        if self.kurukuru_state == 0:
            self.kurukuru_state = 1
            # より遠い方の頂点に向かって辺上を進む
            if abs(ship.position.x) == 48 and abs(ship.position.y) == 48:
                d = Vector(-sign(ship.position.x) * 2, -sign(ship.position.y))
                return Accelerate(ship.ship_id, d)
            if abs(ship.position.x) == 48:
                d = Vector(-sign(ship.position.x), sign(ship.position.y))
                return Accelerate(ship.ship_id, d)

            d = Vector(sign(ship.position.x), -sign(ship.position.y))
            return Accelerate(ship.ship_id, d)

        remain_len = 0
        if ship.velocity.y == 0:
            si = sign(ship.velocity.x)
            tx = ship.position.x
            while abs(tx) < 48:
                tx += si
                remain_len += 1

            if remain_len in [26, 24, 21, 17, 12, 6]:
                # チェックポイントを通過するときは加速する
                self.kurukuru_state += 1
                d = Vector(-si, -sign(ship.position.y))
                return Accelerate(ship.ship_id, d)

            d = Vector(0, -sign(ship.position.y))
            return Accelerate(ship.ship_id, d)
        elif ship.velocity.x == 0:
            si = sign(ship.velocity.y)
            ty = ship.position.y
            while abs(ty) < 48:
                ty += si
                remain_len += 1

            if remain_len in [26, 24, 21, 17, 12, 6]:
                # チェックポイントを通過するときは加速する
                self.kurukuru_state += 1
                d = Vector(-sign(ship.position.x), -si)
                return Accelerate(ship.ship_id, d)

            d = Vector(-sign(ship.position.x), 0)
            return Accelerate(ship.ship_id, d)

        # ここに到達するのはバグだけど一応返しておく
        print("バグだよ～")
        d = Vector(-sign(ship.position.x), -sign(ship.position.y))
        return Accelerate(ship.ship_id, d)

    @classmethod
    def init_ship_parameter(cls, role: int) -> ShipParameter:
        return ShipParameter(
            fuel=350,
            # 攻撃しないので
            power=0,
            regeneration=8,
        )


class ManyAvatarAi(AiBase):

    def think(self) -> List[CommandBase]:
        pos_list = []
        mother_ship = None
        for ship in self.state.ships:
            if self.game_info.role != ship.role:
                continue
            if ship.parameter.fuel == 0:
                pos_list.append([ship.position, ship.velocity])
            else:
                mother_ship = ship
        if mother_ship:
            cmd = self.think_ship(mother_ship, pos_list)
            if cmd is not None:
                return [cmd]
        return []

    def think_ship(self, mother_ship: Ship, pos_list: List[List[Vector]]):
        rest = 384 - self.state.game_tick
        best = will_collision(
            mother_ship.position,
            mother_ship.velocity,
            rest,
        )
        best_a = Vector(0, 0)
        pos = [mother_ship.position, mother_ship.velocity]
        if pos not in pos_list:
            if mother_ship.parameter.p4 > 1 and best >= rest - 1:
                return Separate(
                    mother_ship.ship_id,
                    ShipParameter(
                        fuel=0,
                        power=0,
                        regeneration=0,
                        p4=1,
                    ),
                )
        else:
            best = 1
        aa = [Vector(ax, ay)
              for ax in [1, 0, -1]
              for ay in [1, 0, 1]]
        shuffle(aa)
        for a in aa:
            if a == Vector(0, 0):
                continue
            v = mother_ship.velocity - a
            if update(mother_ship.position, v) == (mother_ship.position, v):
                continue
            for b in aa:
                c = will_collision(
                    mother_ship.position,
                    v - b,
                    rest,
                )
                print(a, b, c)
                if c > best:
                    best = c
                    best_a = a
        print('best:', best, best_a)
        if best_a != Vector(0, 0):
            return Accelerate(mother_ship.ship_id, best_a)

    @classmethod
    def init_ship_parameter(cls, role: int) -> ShipParameter:
        return ShipParameter(
            fuel=210,
            # 攻撃しないので
            power=0,
            regeneration=16,
            p4=20,
        )


def main():
    run(ManyAvatarAi)


if __name__ == '__main__':
    main()

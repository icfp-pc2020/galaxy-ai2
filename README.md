# Team cirijako ICFP-PC2020 main repositry

## Member

Member from rom e-Seikatsu Co. ltd

* @hinhi
* @j_234ko
* @nola_suz
* @scor-A
* @akagenorobin

## About this repositry

* Copy from https://github.com/icfpcontest2020/starterkit-python
* `inp/` is Lisp interpreter implemented by python
* `app/` is AI code for Full Contest
    * main.py: entrypoint and implement AI core
    * game.py: interface for game server
    * gravity.py: simulator of gravity and equation of motion

## Our final strategy

* Use same strategy attacker/efender.
* Just focus on surviving until the end.
* Split the mother ship into 20 ships.

## Why galaxy-ai `2`

We had some mistakes, so recreate the repositry.

## 日時

7/17(金) 22:00 〜 7/20(月) 22:00 JST

## Reference

- 公式: https://icfpcontest2020.github.io/
- meet: https://meet.google.com/add-zwjq-qfm
- スコアボード: https://icfpcontest2020.github.io/#/scoreboard
- docs: https://message-from-space.readthedocs.io/en/latest/index.html
- repos: https://gitlab.com/icfp-pc2020/galaxy-ai2
